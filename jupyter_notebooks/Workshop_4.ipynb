{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 4: Pandas GroupBy\n",
    "by Leslie Kerby"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Pandas GroupBy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`groupby` operations typically involve one or more of the following:\n",
    "- Splitting the Object\n",
    "- Applying a function (aggregation, transformation, filtration)\n",
    "- Combining the results\n",
    "\n",
    "Let's try this out on our NBA Salary dataset from Part 2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "salarydf = pd.read_csv('NBA_season1718_salary.csv').drop('Unnamed: 0', axis=1)\n",
    "salarydf.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "salarydf.groupby('Tm')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This creates a DataFrame GroupBy object. Let's look at it with the `groups` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "salarydf.groupby('Tm').groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's print out the elements of each group (Team) with a `for` loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for team, group in salarydf.groupby('Tm'):\n",
    "    print(team)\n",
    "    print(group)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can do lots of interesting things with these groups/Teams. We can find the total salary paid to NBA players in each Team. We can find the average salary. We can find the number of NBA players on each Team. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for team, group in salarydf.groupby('Tm'):\n",
    "    print(team)\n",
    "    print(group['season17_18'].sum())\n",
    "    print(group['season17_18'].mean())\n",
    "    print(len(group))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Aggregations**<br/>\n",
    "There is a very simple way to do what we just did using `agg` (aggregations). We first use `groupby` to split our data into groups (in this case, for each team). Then we apply the `agg` method to our groups, passing it the aggregations we would like to perform. Passing `np.sum` will sum each numerical column of our groups (in this case we only have one numerical column--'season17_18'). `agg` returns a DataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouped = salarydf.groupby('Tm')\n",
    "grouped.agg(np.sum)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can pass more than one aggregation using a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouped = salarydf.groupby('Tm')\n",
    "grouped.agg([np.sum, np.mean, np.size])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Should we have more than one numerical column in our grouped DataFrame, we can specify which one to aggregate by indexing it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouped = salarydf.groupby('Tm')\n",
    "grouped['season17_18'].agg([np.sum, np.mean, np.size])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that when specifying the column, the header `season17_18` disappeared. Now we can assign our grouped aggregations to a new DataFrame and create plots of the summary statistics using `plot.bar`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouped = salarydf.groupby('Tm')\n",
    "summary = grouped['season17_18'].agg([np.sum, np.mean, np.size])\n",
    "summary.index.name = 'Team' # To rename the title of the index, if you want\n",
    "summary.plot.bar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Transformations**<br/>\n",
    "Covering transformations is outside the scope of this Workshop, but they have the general form\n",
    "```\n",
    "grouped.transform(function)\n",
    "```\n",
    "The function (often a lambda function) is applied to each element in the group. It is useful for normalizing, scaling, or applying PCA on datasets, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Filtrations**<br/>\n",
    "Filtrations have the form\n",
    "```\n",
    "grouped.filter(function)\n",
    "```\n",
    "The function is similar to a Boolean Mask in that it returns a bool, and only the 'True' elements are kept. Again, the function is often a lambda function. Note that what is passed into the `filter` function is each group.\n",
    "\n",
    "As an example, let's keep only the teams that had more than 22 players. `filter` returns a DataFrame similar to the original (but just filtered) as opposed to a Groupby DataFrame object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouped = salarydf.groupby('Tm')\n",
    "grouped.filter(lambda x : len(x) > 22)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use `describe` on the Groupby DataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouped.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can create multiple `groupby` layers. Our NBA salary dataset has no useful second group (as both Player and Salary are all unique values). So let's show this on our Canadian public employees' salary dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "income = pd.read_csv('en-2016-pssd-compendium.csv')\n",
    "income.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3.1**<br/>\n",
    "Use `apply` to create a new column called 'Salary ($)' which contains salary as a number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "income['Salary ($)'] = income['Salary Paid'].apply(lambda x : float(x.strip('$').replace(',','')))\n",
    "income.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3.2** <br/>\n",
    "First, `groupby` just Employer and save it as `emp_groups`. Print out the name and details of the groups with a `for` loop like we did the NBA Salary dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "emp_groups = income.groupby('Employer')\n",
    "for employer, group in emp_groups:\n",
    "    print(employer)\n",
    "    print(group)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3.3**<br/>\n",
    "Use `describe` on `emp_groups`. This may take awhile to run as it is performing several summary statistics on a significant dataset (over 100k rows)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "emp_groups.describe()  # This may take awhile"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3.4**<br/>\n",
    "Use `agg` on `emp_groups['Salary ($)']` to aggregate each Employer by `np.sum`, `np.mean`, `np.std`, `np.size`. Save it as `income_summary`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "income_summary = emp_groups['Salary ($)'].agg([np.sum,np.mean,np.std,np.size])\n",
    "income_summary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 3.5**<br/>\n",
    "Use a Boolean Mask to filter only the Employers with 1000 or more employees in `income_summary`. Then print a bar plot of the total salaries (of employees making over CAN$100,000) of these large Employers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "income_summary[ income_summary['size'] > 999 ]['sum'].plot.bar()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will `groupby` multiple columns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "emp_job_groups = income.groupby(['Employer','Job Title'])\n",
    "emp_job_groups.describe() # Again this may take awhile"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "emp_job_groups.agg([np.sum,np.mean,np.size])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for emp_job, group in emp_job_groups:\n",
    "    print(emp_job)\n",
    "    print(group)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for emp_job, group in emp_job_groups:\n",
    "    print(emp_job[0])\n",
    "    print(emp_job[1])\n",
    "    print(group)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Changing Column Labels or Row Indices**<br/>\n",
    "Use a dictionary and the Pandas DataFrame method `rename`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "salarydf.rename(columns={'Tm':'Team'}, inplace=True)\n",
    "salarydf.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change multiple columns\n",
    "salarydf.rename(columns={'Tm':'Team','season17_18':'Salary'}, inplace=True)\n",
    "salarydf.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use index={dictionary} to change row index\n",
    "salarydf.rename(columns={'Tm':'Team','season17_18':'Salary'},index={0:'zero'}, inplace=True)\n",
    "salarydf.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Can also pass a function instead of a dictionary\n",
    "#salarydf.rename(columns={'Tm':'Team','season17_18':'Salary'},index={'zero':0}, inplace=True)\n",
    "salarydf.rename(index=lambda x: x+1, inplace=True)\n",
    "salarydf.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use `df.columns = [list]` or `df.set_index = [list]` however you must pass a list of all column labels or all row indices."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
