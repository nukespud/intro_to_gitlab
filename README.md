

* Note: The majority of this tutorial comes from Gitlab https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc/gitlab-basics

# GitLab basics

Step-by-step guides on the basics of working with Git and GitLab.

- [Command line basics](shell/README.md)
- [Command line basics for git](command-line-commands.md)
- [Start using Git on the command line](start-using-git.md)
- [Create a project](create-project.md)
- [Fork a project](fork-project.md)
- [Create a merge request](add-merge-request.md)
- [Additional Notes](gitLab.md)
